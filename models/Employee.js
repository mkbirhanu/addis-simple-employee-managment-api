const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Employee = new Schema(
    {
        name: { type: String, required: true },
        date_of_birth: { type: Date, required: true },
        salary: { type: Number, required: true },
        gender: { type: String, enum: ["M","F"], required: true },      
    },
    {
        collection: 'employees'
    },
    { timestamps: true },
)

module.exports = mongoose.model('employees', Employee)