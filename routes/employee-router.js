const express = require('express')

const EmployeeController = require('../controllers/employee-controller')

const router = express.Router()

router.post('/employee', EmployeeController.createEmployee)
router.put('/employee/:id', EmployeeController.updateEmployee)
router.delete('/employee/:id', EmployeeController.deleteEmployee)
router.get('/employee/:id', EmployeeController.getEmployeeById)
router.get('/employees', EmployeeController.getEmployees)

module.exports = router 