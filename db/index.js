const mongoose = require('mongoose')
const MONGO_URL = 'mongodb+srv://employee-user:employee@cluster0.e3xet.gcp.mongodb.net/employee_management_db?retryWrites=true&w=majority'
mongoose
    .connect(MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true  })
    .catch(e => {
        console.error('Connection error', e.message)
    }) 
const db = mongoose.connection

module.exports = db